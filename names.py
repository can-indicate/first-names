"""
The names are retrieved from:
 - https://www2.census.gov/topics/genealogy/1990surnames/dist.male.first
 - https://www2.census.gov/topics/genealogy/1990surnames/dist.female.first
These files are in the public domain.

"""


male_names = []
female_names = []
files = {"./dist.male.first": male_names, "./dist.female.first": female_names}
for key in files.keys():
    with open(key) as f:
        for line in f:
            name = line.split(' ')[0].lower()
            name = name[0].upper() + name[1:]
            files[key].append(name)


with open('male_names.txt', 'w') as f:
    for name in male_names:
        f.write(name + '\n')


with open('female_names.txt', 'w') as f:
    for name in female_names:
        f.write(name + '\n')


names_gd_text = """
extends Node2D

const MALE_NAMES = [
"""

names_gd_text2 = """    "Doggy Dog"]

const FEMALE_NAMES = [
"""

names_gd_text3 = """    "Dogantha"]

"""


with open('names.gd', 'w') as f:
    f.write(names_gd_text)
    for name in male_names:
        f.write("\t\"" + name + '\",\n')
    f.write(names_gd_text2)
    for name in female_names:
        f.write("\t\"" + name + '\",\n')
    f.write(names_gd_text3)
