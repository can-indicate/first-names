# Parsing First Names

This project contains text files of common first names and a python script
to parse it into new files.

## Sources:
The following resources are in the public domain.

*  [male first names](https://www2.census.gov/topics/genealogy/1990surnames/dist.male.first)
*  [female first names](https://www2.census.gov/topics/genealogy/1990surnames/dist.female.first)

